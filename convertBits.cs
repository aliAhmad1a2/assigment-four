using System.Drawing;
using System.Drawing.Imaging;
namespace AssigmentFour;
public class convertBits
{
    public static void convert1BitInto8bit(Bitmap originalBitmap)
    {
        
        Bitmap newBitmap = new Bitmap(originalBitmap.Width, originalBitmap.Height, PixelFormat.Format8bppIndexed);
        
        ColorPalette palette = newBitmap.Palette;
        for (int i = 0; i < 256; i++)
        {
            palette.Entries[i] = Color.FromArgb(i, i, i);
        }

        newBitmap.Palette = palette;

        Rectangle rect = new Rectangle(0, 0, originalBitmap.Width, originalBitmap.Height);
        BitmapData originalData = originalBitmap.LockBits(rect, ImageLockMode.ReadOnly, PixelFormat.Format1bppIndexed);
        BitmapData newData = newBitmap.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

        int originalStride = originalData.Stride;
        int newStride = newData.Stride;
        
        unsafe
        {

            int  or = originalData.Height;
            int o = originalData.Width;
            
            for (int y = 0; y <or ; y++)
            {
                byte * originalRow = (byte *)originalData.Scan0 + y * originalStride;
                byte * newRow = (byte *)newData.Scan0 + y * newStride;
                
                for (int x = 0; x < o; x++)
                {
                    int originalColor = (originalRow[x / 8] >> (7 - (x % 8))) & 0x01;//the result from this code is 0,1
                    int newColor = originalColor * 255;
                    newRow[x] = (byte)newColor;    
                  
                    
                }
            }
        }
        originalBitmap.UnlockBits(originalData);
        newBitmap.UnlockBits(newData);
        newBitmap.Save("../../../Images/newImage1.jpg");

    }

    
}